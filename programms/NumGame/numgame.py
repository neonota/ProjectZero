#Guess the  number
import random
#Welcome User
username = input("What's your name? : ")
print("Welcome to our number guessing game," , username + "!")

top_of_range = input("Type a number: ")

while True:
    if top_of_range.isdigit():
        top_of_range = int(top_of_range)
        if top_of_range <= 0:
            print("Type a number larger than 0")
            quit()
    else:
        print("Please type a number")
        continue
    r1 = random.randint(0,top_of_range)
    g1 = 0
#print (r1)

    while True:
        g1 += 1
        user_guess = input("Make a guess: ")
        if user_guess.isdigit():
            user_guess = int(user_guess)
        else:
            print ("Please type a number next time")
            continue
        if user_guess == r1:
            print("You Got It!")
            break
        else:
            if user_guess > r1:
                print("You were above the number!")
            else:
                print("You were below the number")

    continue

#Result
print("You got it in", g1, "guesses")
