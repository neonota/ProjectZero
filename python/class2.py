#Class Attributes
class Student:
    def __init__ (self, id_number, name, age):
        self.id_number = id_number
        self.name = name
        self.age = age

#Creating an instance of the class
#An instance is simply a object created from a class
st1 = Student(123, "Ruskel" , 20)
st2 = Student(456, "Jaber" , 21)

#Access the prop of the instance 
p = st2.id_number
q = st2.name
r = st2.age

print (p)

#QN
#Previously I made a mistake on line 3. I used "_init_" instead of "__init__". Which resulted a type error
