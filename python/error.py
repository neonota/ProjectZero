# Recieve input from the user and show custom error message if the input type is not valid

#while True:
#    try:
#        age = int(input("How old are you ? :"))
#    except ValueError:
#       print("Please enter a number for your age.")
#    else:
#        break

#print(f"Your age will be {age+1} next year.")

# Raise Exception if conditions are not met 

number = 10
#if number < 20:
#    raise Exception("The number is less than 20.")
#print(number)

# AssertError

assert (number < 5), f"The Number should not exceed. ({number})"

print(number)
