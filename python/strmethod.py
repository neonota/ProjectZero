#Capitalize String
text="hello World ! You're pure evil"
x = text.capitalize()
print (x) #This is capitalize the first letter of the first word of the sentence

#Convert to upper case
y = text.upper()
print (y) #It will return a copy of the given string but all in a upper case

#Convert to lower case
z = y.lower()
print (z)

#Get length of the string 
t1 = "The length of the string is,"
len1 = len(z)
print (t1,len1)

#Replace part of the string
rep1 = y.replace("WORLD","Everyone")
print (rep1)

#Check if a value is present in a string
valcheck = "World" in text
print (valcheck)
