#Dictionary
x = {
        "First_Name": "John",
        "Last_Name" : "Doe",
        "Age" : 28,
        }
print (x)

# Accessing Items
first = x["First_Name"]
print ("His first name is, ", first)

#Add an item
x["Hobby"] = "Playing CODM"

print (x)

# Checking the existence of an item

hobby = x["Hobby"] 
if "Hobby" in x:
    print ("His hobby is", hobby)
